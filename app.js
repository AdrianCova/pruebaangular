var createError = require('http-errors');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors')
const fs = require('fs');
const readline = require('readline');

var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
}

var apiRouter = require('./routes/fanPage');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cors(corsOptions))
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'dist/angular-app')));
app.use('/', express.static(path.join(__dirname, 'dist/angular-app')));
app.use('/api', apiRouter);
app.route('/api/lista/:name').get((req, res) => {
  const requestedPais = req.params['name'];
  
  try {
    const rl = readline.createInterface({
      input: fs.createReadStream('./src/assets/lista' + requestedPais + '.txt'),
      crlfDelay: Infinity
    });
    jsonResponse = "[";
    rl.on('line', (line) => {
      //console.log('Line from file: ' +  linea);
      let elems = line.split("->");
      //console.log(elems);
      //console.log("1: " + elems[0]);
      //console.log("2: " + elems[1].trim());
      jsonResponse += '{"fans": "' + elems[1].trim() + '", "nombre": "' + elems[0].trim() + '"}, ';
    }).on('close', function() {
      jsonResponse = jsonResponse.substring(0, jsonResponse.length - 2);
      jsonResponse += "]";
      console.log(jsonResponse);
      res.send(jsonResponse);
    });
  } catch (err){
    console.log("Ocurrió un error leyendo el archivo con la lista");
    res.send('Ocurrió un error');
  }
});

app.route('/api/cats').post((req, res) => {
  res.send(201, req.body);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err.status);
});

module.exports = app;