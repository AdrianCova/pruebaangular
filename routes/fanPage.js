var express = require('express');
var router = express.Router();
var puppeteer = require('puppeteer');
var fs = require('fs');

//paginas mexico,united-states,spain
async function runLista(pagina) {
	var browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});//puppeteer.launch();
	var page = await browser.newPage();
	var url = 'https://www.socialbakers.com/statistics/facebook/pages/total/' + pagina + '/';
	var LIST_PAGENAME_SELECTOR = 'body > div.page > div.content.content--subpages > div > section > section > div > table > tbody > tr:nth-child(INDEX) > td.name > div > a > h2 > span.show-name';
	var LIST_FANS_SELECTOR = 'body > div.page > div.content.content--subpages > div > section > section > div > table > tbody > tr:nth-child(INDEX) > td:nth-child(3) > div > strong';
	var LENGTH_SELECTOR_CLASS = 'show-name';
	
	await page.goto(url);
	await page.waitFor(2*1000);
	let listLength = await page.evaluate((sel) => {
		return document.getElementsByClassName(sel).length;
	}, LENGTH_SELECTOR_CLASS);
	//console.log(listLength);
	let textoFinal = "";
	for (let i = 1; i <= listLength; i++) {
	    // change the index to the next child
	    let pagenameSelector = LIST_PAGENAME_SELECTOR.replace("INDEX", i);
	    let fansSelector = LIST_FANS_SELECTOR.replace("INDEX", i);

	    let pagename = await page.evaluate((sel) => {
	    	let nombre = document.querySelector(sel).innerHTML;
	      	nombre = nombre.trim();
	        return nombre;
	    }, pagenameSelector);

	    let fans = await page.evaluate((sel) => {
	    	let total = document.querySelector(sel).innerHTML;
	    	total = total.trim();
	    	total = total.replace(new RegExp("&nbsp;", 'g'), ",");
	        return total;
	    }, fansSelector);

	    console.log(pagename, ' -> ', fans);
	    textoFinal += pagename + ' -> ' + fans + "\n";
	}
	fs.writeFile("./src/assets/lista" + pagina +".txt", textoFinal, function(err) {
	    if(err) {
	        return console.log(err);
	    }
	    console.log("The file was saved!");
	}); 
	await browser.close();
}

/* GET listas. */
router.get('/', function(req, res, next) {
	runLista("mexico");
	runLista("united-states");
	runLista("spain");
	//res.param('prueba') = "prueba respuesta";
	//var htmlRedirect = 'Se cargaron correctamente las listas! <br> <a href="/" >Volver a pantalla principal</a>';
	//res.redirect('/?success=true');
	res.status(200).send("Se ha iniciado el proceso de llenado correctamente! Tardará unos segundos.");
	//res.send({
		//fanPages: [{ name: 'Pagina1' }, { name: 'Pagina2' }]
	//});
});

module.exports = router;