const puppeteer = require('puppeteer');
const fs = require('fs');

//paginas mexico,united-states,spain
async function runLista(pagina) {
	const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});//puppeteer.launch();
	const page = await browser.newPage();
	const url = 'https://www.socialbakers.com/statistics/facebook/pages/total/' + pagina + '/';
	const LIST_PAGENAME_SELECTOR = 'body > div.page > div.content.content--subpages > div > section > section > div > table > tbody > tr:nth-child(INDEX) > td.name > div > a > h2 > span.show-name';
	const LIST_FANS_SELECTOR = 'body > div.page > div.content.content--subpages > div > section > section > div > table > tbody > tr:nth-child(INDEX) > td:nth-child(3) > div > strong';
	const LENGTH_SELECTOR_CLASS = 'show-name';
	
	await page.goto(url);
	await page.waitFor(2*1000);
	let listLength = await page.evaluate((sel) => {
		return document.getElementsByClassName(sel).length;
	}, LENGTH_SELECTOR_CLASS);
	//console.log(listLength);
	let textoFinal = "";
	for (let i = 1; i <= listLength; i++) {
	    // change the index to the next child
	    let pagenameSelector = LIST_PAGENAME_SELECTOR.replace("INDEX", i);
	    let fansSelector = LIST_FANS_SELECTOR.replace("INDEX", i);

	    let pagename = await page.evaluate((sel) => {
	    	let nombre = document.querySelector(sel).innerHTML;
	      	nombre = nombre.trim();
	        return nombre;
	    }, pagenameSelector);

	    let fans = await page.evaluate((sel) => {
	    	let total = document.querySelector(sel).innerHTML;
	    	total = total.trim();
	    	total = total.replace(new RegExp("&nbsp;", 'g'), ",");
	        return total;
	    }, fansSelector);

	    console.log(pagename, ' -> ', fans);
	    textoFinal += pagename + ' -> ' + fans + "\n";
	}
	fs.writeFile("./src/assets/lista" + pagina +".txt", textoFinal, function(err) {
	    if(err) {
	        return console.log(err);
	    }
	    console.log("The file was saved!");
	}); 
	await browser.close();
}

runLista("mexico");
runLista("united-states");
runLista("spain");