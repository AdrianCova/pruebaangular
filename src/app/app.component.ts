import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

	paisSeleccionado: string;

	paises = [
		{ clave: 'mexico', descripcion: 'México' },
		{ clave: 'spain', descripcion: 'España' },
		{ clave: 'united-states', descripcion: 'Estados Unidos' }
	];

	paginas: any = [];

	constructor(public http : Http) {

	}

	buscarFanPage(){
		//this.http.get('http://localhost:3000/api/lista/' + this.paisSeleccionado)
		this.http.get('https://prueba-angular-scrap.herokuapp.com/api/lista/' + this.paisSeleccionado)
		.map(res => res.json())
		.subscribe(data =>
		{
			this.paginas = data;
		});
		//this.paginas = [{ fans: '150', nombre: this.paisSeleccionado }, { fans: '120', nombre: 'Pag2' },{ fans: '100', nombre: '3raPag' }];
	}


}
